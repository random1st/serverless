#!/bin/bash


setup () {
    echo  ------- SETUP -------
    apt-get update # required to install zip
    apt-get install -y zip
    pip install virtualenv
    virtualenv --python=python3 env
    source env/bin/activate
    pip install -r requirements.txt
    return $?
}


tests() {
    echo ------- TESTS -------
    pip install -r requirements-test.txt # for tests
    tox
    return $?
}

deploy() {
    echo ------- DEPLOY -------
    echo $1
    pip install awscli
    aws s3 cp s3://$CMDB/zappa_settings.json .
    zappa update $1 || zappa deploy $1
    zappa certify $1 --yes
    zappa manage $1 "migrate --noinput"
    zappa manage $1 "collectstatic --noinput"
    return $?
}

setup && test && deploy $1



